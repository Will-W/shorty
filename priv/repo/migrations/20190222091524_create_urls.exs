defmodule Shorty.Repo.Migrations.CreateUrls do
  use Ecto.Migration

  def change do
    create table(:urls) do
      add :token, :string
      add :url, :string

      timestamps()
    end

    create unique_index(:urls, [:token])
  end
end
