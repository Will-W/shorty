# Shorty

This repo is my code from the 'Elixir for Rubyists' meetup.

I re-ran the steps from that presentation, performing git commits at each stage so that the progression is clear. I have also added a couple of small improvements over what I demonstrated, to make it more like real code:

* Error handling for invalid tokens on the redirect path
* A tiny convenience view function for the urls in the 'show' page
* A redirect from `/` to `/urls` to save some address bar fiddling

## Usage

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

If you forget what is where, you can always run `mix phx.routes` to show all the routes in the application.

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
