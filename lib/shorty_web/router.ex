defmodule ShortyWeb.Router do
  use ShortyWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ShortyWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/urls", UrlController
    get "/r/:token", UrlController, :redir
  end

  # Other scopes may use custom stacks.
  # scope "/api", ShortyWeb do
  #   pipe_through :api
  # end
end
