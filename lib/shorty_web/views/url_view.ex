defmodule ShortyWeb.UrlView do
  use ShortyWeb, :view

  @doc """
  Create a link who's text is the same as the link target
  """
  def link_and_text(str) do
    link(str, to: str)
  end
end
