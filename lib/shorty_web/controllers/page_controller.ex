defmodule ShortyWeb.PageController do
  use ShortyWeb, :controller

  def index(conn, _params) do
    redirect(conn, to: Routes.url_path(conn, :index))
  end
end
