defmodule Shorty.Urls.Url do
  use Ecto.Schema
  import Ecto.Changeset


  schema "urls" do
    field :token, :string
    field :url, :string

    timestamps()
  end

  @doc false
  def changeset(url, attrs) do
    url
    |> cast(attrs, [:token, :url])
    |> validate_required([:token, :url])
    |> validate_format(:url, ~r(^https?://), message: "Must be fully qualified domain name")
  end
end
